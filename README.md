# Dejar de seguir masivo para Instagram (Español)

## ¿Cómo se usa?

1. Inicia sesión en Instagram y ves al muro donde ves tus publicaciones.
2. En tu navegador favorito arranca la consola para JavaScript. 
(En Firefox pulsa Opciones / Desarrollador web / Consola web). 
3. Pega todo el código de Dejar_de_seguir_masivo-Instagram.js en la consola y pulsa Intro.
4. Esperar y disfrutar de la mágia.

~~~ 
He notado que a veces hay que recargar la web y probar de nuevo o intentarlo de nuevo pasado un tiempo. 
Parece que Instagram deja de hacer caso a las peticiones de dejar de seguir si son muchas y continuadas.
~~~
