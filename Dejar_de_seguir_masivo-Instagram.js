/**
 * Instagram JavaScript unfollow
 *
 * WHAT IS IT?
 * Unfollow people in the instagram website
 *
 * HOW TO:
 * Log in to Instagram, use your web browser's JavaScript console 
 * and paste the code. Enjoy the magic. 
 *
 * Created on 15/03/17.
 * @author Joel Hernandez <involvmnt@gmail.com>
 * Modified on 10/09/2018 by Marcos Martín <marcos.martingm@gmail.com>
 */

openFollowersWindow().then(function () {
    populateUnfollowsPool();
    digestUnfollowsPool();
});

function openFollowersWindow() {
    var onFollowersWindowWasOpenedListeners = [];
    var openWindowTimeout = 3000;

    var followersElement = getFollowersElement();
    followersElement.click();

    function digestOnFollowersWindowWasOpenedListeners() {
        onFollowersWindowWasOpenedListeners.forEach(function (onFollowersWindowWasOpenedListener) {
            onFollowersWindowWasOpenedListener();
        });
    }

    var wasOpened;
    setTimeout(function () {
        // TODO Verify that the window was indeed opened
        wasOpened = true;
        digestOnFollowersWindowWasOpenedListeners();
    }, openWindowTimeout);
    return {
        then: function (onFollowersWindowWasOpened) {
            if (wasOpened) {
                onFollowersWindowWasOpened();
            } else {
                onFollowersWindowWasOpenedListeners.push(onFollowersWindowWasOpened);
            }
        }
    };
}

function getFollowersElement() {
    return getFollowersElementWithUsername(getUsername());
}

function getUsername() {
    var pageTitleElement = document.getElementsByTagName('h1')[0];
    if (!pageTitleElement) throw new Error('No title to get username from');
    return pageTitleElement.innerHTML;
}

function getFollowersElementWithUsername(username) {
    var followersElement = document.querySelectorAll('a[href="/' + username + '/following/"]')[0];
    if (!followersElement) throw new Error('No followers element was found');
    return followersElement;
}

var unfollowsPool;

function populateUnfollowsPool() {
    var buttons = document.getElementsByTagName('button');
    unfollowsPool = [];
    for (var i = 0; i < buttons.length; i++) {
        var button = buttons[i];
        if (button.innerHTML.includes('Siguiendo')) {
            var randomTimeoutForUnfollow = Math.floor((Math.random() * 10) + 1) * 1000;
            console.log('Following button!');

            var unfollow = {
                buttonElement: button,
                timeout: randomTimeoutForUnfollow
            };

            unfollowsPool.push(unfollow);
        }
    }
}

function digestUnfollowsPool() {
    if (unfollowsPool.length === 0) {
        console.log('Unfollow pool empty, repopulating');
        populateUnfollowsPool();
    }
    var unfollow = unfollowsPool.shift();
    var unfollowTimeout = unfollow.timeout/1000;
    console.log('Clicking unfollow button in', unfollowTimeout);
    setTimeout(function () {
        var unfollowButtonElement = unfollow.buttonElement;
        unfollowButtonElement.scrollIntoView(true);
        console.log('Clicking unfollow button');
        unfollowButtonElement.click();
		confirm();
        digestUnfollowsPool();
    }, unfollowTimeout);
}

// Need confirm unfollow
function confirm() {
	var aTags = document.getElementsByTagName("button");
	var searchText = "Dejar de seguir";
	var found;
	wait(3000);  //3 seconds in milliseconds
	for (var i = 0; i < aTags.length; i++) {
	  if (aTags[i].textContent == searchText) {
		found = aTags[i];
		found.click();
		console.log('Clicking confirm. Continuing ...');
		break;
	  }
	}
}

function wait(ms){
   var start = new Date().getTime();
   var end = start;
   while(end < start + ms) {
     end = new Date().getTime();
  }
}